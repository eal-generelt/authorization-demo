﻿USE [AuthorizationDemo]
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'C1C37E6A-E8D8-4980-A344-A45BC51FBC2A', N'Administrator', N'ADMINISTRATOR', NULL)
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount]) VALUES (N'74d9a7f8-c274-48aa-a15d-63ee9a5c667a', N'kbr@ucl.dk', N'KBR@UCL.DK', N'kbr@ucl.dk', N'KBR@UCL.DK', 0, N'AQAAAAEAACcQAAAAECJy/ZipHdwESO1+2ImsWuout7mwpJ0G8eH0IPwCT1vTxaNspaStRj1gOttm/FKOkQ==', N'YVPBZ2FCC3VRW2ZD4T2EOTP2HSALRDKR', N'6727aea5-cc7c-479b-beb5-9e8198c5d97a', NULL, 0, 0, NULL, 1, 0)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'74d9a7f8-c274-48aa-a15d-63ee9a5c667a', N'C1C37E6A-E8D8-4980-A344-A45BC51FBC2A')
GO